# DevelopmentEnvironmentImage

## Description
This repository contains vim configuration with all vim plugins needed for development
and docker image which opens vim inside configured environment with code folder mounted.
More details on package list can be found [here](https://codeberg.org/YarikMamykin/vim_config)

### Build arguments
- DISTRO = name of base distribution (e.g. archlinux, ubuntu)
- ROOT_REPO = link to host, that contains dependencies for container (bash_config, vifm_config)
- GIT_NAME = name for git user
- GIT_EMAIL = email for git user

### Build cmd
Image is build for **rootless** docker only!

This image can be built within next command (e.g. for archlinux):
```
docker build \
	--build-arg DISTRO=archlinux \
	--build-arg ROOT_REPO="https://codeberg.org/YarikMamykin" \
	--build-arg GIT_NAME=gitname \
	--build-arg GIT_EMAIL="somemail@domain.com" \
	-t vim-arch-dev .
```

And for docker compose (for rootless docker replace devenv with devenv-rootless):
```
docker compose build vimdev \
    --build-arg DISTRO=archlinux 
    --build-arg ROOT_REPO="https://codeberg.org/YarikMamykin" 
	--build-arg GIT_NAME=gitname \
	--build-arg GIT_EMAIL="somemail@domain.com" 
```

### Important
In case of `docker compose` usage it is required to have `codebase` and `ssh-certs` docker volumes pre-created.
E.g. `docker volume create --name codebase --opt type=none --opt o=bind --opt device=<path_to_your_code_folder>`

### Run
To run container use one of these commands:
- `docker run --rm -ti vimdev` 
- `docker compose run vimdev`
