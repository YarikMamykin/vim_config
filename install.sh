#! /bin/bash

tar -xvf ./vim.91.tar.gz
pushd vim/
  make configure
  make -j`nproc`
  sudo make install
popd
rm -frv vim/

LINUX_CONFIG_DIR="${PWD}"
VIM_CONFIG="${LINUX_CONFIG_DIR}/vimcfg"

# Delete old configurations
if [ -d "${HOME}/.vim" ]; then
  rm -frv "${HOME}/.vim"
fi

if [ -d "${HOME}/.config/coc" ]; then
  rm -frv "${HOME}/.config/coc"
fi

if [ -f "${HOME}/.vimrc" ]; then
  rm -fv "${HOME}/.vimrc"
fi

# Prepare dirs and paths
mkdir -p   ${HOME}/.vim/
mkdir -p   ${HOME}/.vim/autoload
mkdir -p   ${HOME}/.vim/plugged 
mkdir -p   ${HOME}/.vim/colors
mkdir -p   ${HOME}/.vim/syntax
mkdir -p   ${HOME}/.vim/tags
mkdir -p   ${HOME}/.vim/usercfg
mkdir -p   ${HOME}/.config/coc

# install plugin manager
curl -fLo  ${HOME}/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

ln -sf ${VIM_CONFIG}/vimrc          ${HOME}/.vimrc 
ln -sf ${VIM_CONFIG}/custom.vim     ${HOME}/.vim/colors/custom.vim

for cfg_file in "ignorelist" "properties" "commands" "functions" "pluginscfg" "behavior"; do
  ln -sf ${VIM_CONFIG}/${cfg_file}.vim  ${HOME}/.vim/usercfg/${cfg_file}.vim
done

for cfg_dir in "key_mappings"; do
  ln -sf ${VIM_CONFIG}/${cfg_dir} ${HOME}/.vim/usercfg/${cfg_dir}
done

vim -c "source ~/.vimrc | PlugInstall | qa"

pushd $VIM_CONFIG/helpers
  python3 coc-config-gen.py 
  mv coc-settings.json ${HOME}/.vim/coc-settings.json
  mkdir -p ${HOME}/.config/coc/extensions/
popd

vim -c "source ~/.vimrc | CocInstall -sync coc-clangd coc-cmake coc-css coc-html coc-json coc-lua coc-pyright coc-sh coc-tsserver coc-vetur coc-vimlsp coc-prettier coc-go coc-yaml | qa"

echo "Enjoy!"
