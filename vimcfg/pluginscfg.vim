" Tagbar
let g:tagbar_sort = 1
let g:tagbar_autofocus = 1
let g:tagbar_case_insensetive = 0
let g:tagbar_compact = 1
let g:tagbar_width = 100
let g:tagbar_indent = 1
let g:tagbar_show_linenumbers = 0

" Git Gutter
let g:gitgutter_enabled = 1
let g:gitgutter_signs = 1
let g:gitgutter_highlight_lines = 0
let g:gitgutter_async = 1
let g:gitgutter_max_signs = 500
let g:gitgutter_map_keys = 0

" python autocomplete
let g:pydiction_location = '~/.vim/plugged/pydiction/complete-dict'
let g:pydiction_menu_height = 10
" python highlight
let g:python_highlight_all=1

" NERDCommenting sets
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Default mappings
" \cc -> comment current line/highlighted text
" \ci -> toggle comment/uncomment line(s)
" \cs -> sexy commenting (comments line(s) into a comment block)
" \cy -> copy line(s) before commenting and comment
" \c$ -> comment line from cursor position to its end
" \cA -> add comment chars to the end of line and go to insert mode
" \cu -> uncomment line(s)/word

" Taglist
filetype on
let g:Tlist_WinWidth=120
let g:Tlist_Use_Right_Window=1
let g:Tlist_Auto_Update=1
let g:Tlist_Auto_Highlight_Tag=1
let g:Tlist_GainFocus_On_ToggleOpen=1
let g:Tlist_Display_Prototype=1
let g:Tlist_Enable_Fold_Column=1
let g:Tlist_Show_One_File=1

" Explorer settings
let g:netrw_browse_split = 0
let g:netrw_winsize = 60
let g:netrw_banner = 0
let g:netrw_liststyle = 0
let g:netrw_altv = 1
let g:netrw_winsize = 20

" ClangFormat
let g:clang_format#auto_format = 1
let g:clang_format#detect_style_file = 1
