syntax match INFO /^INFO/
syntax match WARN /^WARN/
syntax match DEBUG /^DEBUG/
syntax match ERROR /^ERROR/
syntax match TRACE /^TRACE/
syntax match DATE /\[\d* [a-zA-Z]* \d* \d\d\:\d\d\:\d\d\,\d*\]/
syntax match THREAD /\[\dx[a-z\|0-9]*\]/
syntax match COMPONENT /\[[a-z\|A-Z]*\]/
syntax match FILE / \S\+:\d\+ /

hi INFO        cterm=italic   ctermfg=190
hi WARN        cterm=bold     ctermfg=1
hi DEBUG       cterm=bold     ctermfg=5
hi ERROR       cterm=bold     ctermfg=0       ctermbg=9
hi TRACE       cterm=none     ctermfg=34
hi DATE        cterm=none     ctermfg=33
hi THREAD      cterm=none     ctermfg=200
hi COMPONENT   cterm=bold     ctermfg=118
hi FILE        cterm=none     ctermfg=245

function! SetConceal()
  setlocal concealcursor=ni 
  setlocal conceallevel=2
endfunction

function! UnsetConceal()
  setlocal concealcursor= 
  setlocal conceallevel=0
endfunction

function! ShowAll()
  setlocal concealcursor= 
  setlocal conceallevel=0
  syntax clear LevelConcealed
  syntax clear DateConcealed
  syntax clear ThreadConcealed
  syntax clear ComponentConcealed
  syntax clear FileConcealed
endfunction
command! ShowAll :call ShowAll()

function! HideAll()
  setlocal concealcursor=ni
  setlocal conceallevel=2
  :HideLogLevel
  :HideDate
  :HideThread
  :HideComponent
  :HideFile
endfunction
command! HideAll :call HideAll()

command! HideLogLevel :call SetConceal() | syntax match LevelConcealed /^INFO\|^WARN\|^DEBUG\|^TRACE\|^ERROR/ conceal
command! ShowLogLevel :syntax clear LevelConcealed

command! HideDate :call SetConceal() | syntax match DateConcealed /\[\d* [a-zA-Z]* \d* \d\d\:\d\d\:\d\d\,\d*\]/ conceal
command! ShowDate :syntax clear DateConcealed

command! HideThread :call SetConceal() | syntax match ThreadConcealed /\[\dx[a-z\|0-9]*\]/ conceal
command! ShowThread :syntax clear ThreadConcealed
      
command! HideComponent :call SetConceal() | syntax match ComponentConcealed /\[[a-z\|A-Z]*\]/ conceal
command! ShowComponent :syntax clear ComponentConcealed

command! HideFile :call SetConceal() | syntax match FileConcealed / \S\+:\d\+ / conceal
command! ShowFile :syntax clear FileConcealed

setlocal nonumber
setlocal nowrap
setlocal readonly
