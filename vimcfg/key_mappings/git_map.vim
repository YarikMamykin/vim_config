" Git Gutter
nmap ]p :GitGutterNextHunk<CR>
nmap [p :GitGutterPrevHunk<CR>
" Flog -> get commit from current line and make fixup to it
nmap <space>fc :execute 'normal! ^%%l"cyw'<CR>:Git commit --fixup=<C-r>c<CR>
