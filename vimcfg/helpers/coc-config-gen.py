import json
import subprocess
import codecs
from os import listdir
from os.path import isdir, join, isfile


coc_config_file = "coc-settings.json"

config_json = """ {
    "languageserver": {
        "qml": {
            "filetypes": ["qml"],
            "command": "qmlls6"
        }
    },
    "tsserver.enable": true,
    "codeLens.enable": true,
    "coc.preferences.extensionUpdateCheck": "never",
    "coc.preferences.messageLevel": "error",
    "coc.preferences.formatOnSaveFiletypes": [
      "h",
      "cpp",
      "c",
      "json",
      "yaml",
      "yaml.docker-compose",
      "yml",
      "javascript",
      "typescript",
      "vue",
      "go",
      "css",
      "markdown",
      "python",
      "sh",
      "Dockerfile",
      "html"
    ],
    "clangd.enabled": true,
    "pyright.enable": true,
    "python.formatting.provider": "black",
    "python.linting.enabled": true,
    "python.linting.flake8Enabled": true
}"""

config_data = json.loads(config_json)

include_paths = ["/usr/include", "/usr/local/include"]

clang_lib_path = "/usr/lib/clang"
clang_version_dir = [
    d for d in listdir(clang_lib_path) if isdir(join(clang_lib_path, d))
][0]
include_paths.append(join(clang_lib_path, clang_version_dir, "include"))

clangd_path = [
    f
    for f in listdir("/usr/bin")
    if isfile(join("/usr/bin", f)) and "clangd" in join("/usr/bin", f)
][0]
config_data["clangd.path"] = join("/usr/bin", clangd_path)
config_data["clangd.arguments"] = [
    "--log=error",
    "--background-index",
    "--all-scopes-completion",
    "--limit-results=10",
    "--clang-tidy",
    "--completion-style=detailed",
    "--fallback-style=none",
]

result = subprocess.run(["which", "npm"], stdout=subprocess.PIPE)
npm_path = result.stdout.decode("UTF-8").strip("\n")
config_data["tsserver.npm"] = npm_path

with codecs.open(coc_config_file, "w", "utf8") as f:
    f.write(json.dumps(config_data, sort_keys=True, ensure_ascii=False))
