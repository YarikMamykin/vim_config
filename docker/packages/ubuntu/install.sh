#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive 

apt update
apt install -yq git software-properties-common python3-launchpadlib

git clone ${ROOT_REPO}/distro_packages -b ${DISTRO} ${DISTRO}
pushd ${DISTRO}
	pushd dev
		apt update 
		apt install -yq `cat packages.txt`
	popd
popd
apt install -yq `cat packages.txt`

curl -sL https://deb.nodesource.com/setup_current.x | bash -
apt update
apt install -yq nodejs 
