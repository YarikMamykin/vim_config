#!/bin/bash -e

echo [multilib] | tee -a /etc/pacman.conf
echo Include = /etc/pacman.d/mirrorlist | tee -a /etc/pacman.conf
pacman-key --init
pacman-key --populate archlinux
pacman -Syy 
pacman -S --noconfirm archlinux-keyring 
pacman -Syy 
pacman -S --noconfirm sudo git glibc lib32-glibc

git clone ${ROOT_REPO}/distro_packages -b ${DISTRO} ${DISTRO}
pushd ${DISTRO}
	pushd dev
		pacman -Syy
		pacman --noconfirm -S `cat packages.txt`
	popd
popd
