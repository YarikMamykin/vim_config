#! /bin/bash

# Place anything before the vim launch to be done.
# E.g. source ./.venv-dev for python projects, etc.

source ~/.bashrc
[ -f "/mnt/codebase/user_entry.sh" ] && source /mnt/codebase/user_entry.sh
vim /mnt/codebase/${WORKDIR}
