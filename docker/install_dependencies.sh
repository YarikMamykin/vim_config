#!/bin/bash -e

[ -n "${ROOT_REPO}" ] || exit 1
[ -n "${DISTRO}" ] || exit 1
[ -n "${GIT_NAME}" ] || exit 1
[ -n "${GIT_EMAIL}" ] || exit 1

function install_distro_packages {
	pushd ./packages/${DISTRO}
		chmod +x ./install.sh
		if [ 0 -eq `id -u` ]; then
			./install.sh
		else
			sudo -E ./install.sh
		fi
	popd
}

function install_nodejs_packages {
	pushd ./packages/
		if [ 0 -eq `id -u` ]; then
			npm install -g `cat nodejs_packages.txt`
		else
			sudo npm install -g `cat nodejs_packages.txt`
		fi
	popd
}

function setup_git {
	pushd ${HOME}
		git config --global merge.tool vimdiff 
		git config --global merge.conflictstyle diff3 
		git config --global mergetool.prompt false 
		git config --global user.email "${GIT_EMAIL}" 
		git config --global user.name "${GIT_NAME}"
	popd
}

install_distro_packages
install_nodejs_packages
setup_git

for repo in vifm_config bash_config; do
	git clone ${ROOT_REPO}/${repo}.git
	pushd $repo
		./install.sh
	popd
done

